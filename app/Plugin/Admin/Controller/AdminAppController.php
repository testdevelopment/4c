<?php

App::uses('AppController', 'Controller');

class AdminAppController extends AppController {
    /**
     * Helpers
     *
     * @var array
     */

    public $components = array(
        'Auth' => array(
            'loginAction' => array(
                'controller' => 'admin_users',
                'action'     => 'login',
                'plugin'     => 'admin'
            ),
            'authenticate' => array(
                'Form' => array(
                    'fields'    => array('username' => 'login', 'password' => 'password'),
                    'userModel' => 'AdminUser'
                )
            ),
            'loginRedirect' => array(
                'controller' => 'admin',
                'action'     => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'admin_users',
                'action'     => 'login',
                'plugin'     => 'admin'
            )
        ),
        'Session'
    );
    
    
    /* basic premissions for groupe */
    var $permissions = array(
        'index' => '*'
    );
    
    /**
     * isAuthorized function
     *
     * @return true if Auth admin or action permission allowed for all or Auth->group have allow to action/false in other cases
     */
    public function isAuthorized($user) {
        // admins have access to everything by default
        if($this->Auth->user('group') == 'administrator') return true;
        if(!empty($this->permissions[$this->action])) {
            if($this->permissions[$this->action] == '*') return true;
            if(in_array($this->Auth->user('group'), $this->permissions[$this->action])) return true;
        }
        return false;
    }

    /**
     * Scaffold
     *
     * @var string
     */
    public $scaffold;

    /**
     * Before Filter
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->authorize = array('Controller');
        $this->Auth->deny();
        
        $this->set(
            'preEnrolls',
            ClassRegistry::init('Student')->find(
                'count',
                array('conditions' => array('Student.enrollment_status' => 'Pre-Enrolled'))
            ));
    }
}
?>