<?php

class CoursesController extends AdminAppController {
    var $name = 'Courses';
    var $uses = array('Course', 'Student');
    
    var $permissions = array(
        'index'  => '*',
        'view'   => '*',
        'add'    => array('administrator', 'curator lvl1'),
        'edit'   => array('administrator', 'curator lvl1'),
        'delete' => array('administrator')
    );

    /**
     * list of courses
     */
    public function index($status = false) {
        if($status) {
            $courses = $this->Course->find('all', array(
                'conditions' => array(
                    'Course.active' => $status,
                ),
                'recursive' => 1
            ));
        } else {
            $courses = $this->Course->find('all', array('recursive' => 1));
        }

        $this->set('courses', $courses);
    }
    
    /**
     * view the course
     * 
     * @param int $id
     */
    public function view($id = NULL) {
        if($id == NUll) {
            $this->Session->setFlash(__('Wrong parameter. Try again'), 'flash_error');
            return $this->redirect(array('action' => 'index'));
        } else {
            $course =$this->Course->findById($id);
            $this->set('course', $course);
        }
    }
    
    /**
     * adding the course
     */
    public function add() {
        $this->set('selectOptions', $this->Course->getSelectOptions());

        if($this->request->is('post')) {
            $this->Course->create();
            if($this->Course->save($this->request->data)) {
                $this->Session->setFlash(__('Session added'), 'flash_success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Error. Try again'), 'flash_error');
            }
        }
    }
    
    /**
     * edit course
     * 
     * @param int $id
     */
    public function edit($id = NULL) {
        if($id == NUll) {
            $this->Session->setFlash(__('Wrong parameter. Try again'), 'flash_error');
            return $this->redirect(array('action' => 'index'));
        } else {
            $this->Course->id = $id;
            $this->set('selectOptions', $this->Course->getSelectOptions());
            
            if (empty($this->data)) {
                $this->data = $this->Course->read();
            } else {
                $this->request->data['Course']['modified_by'] = $this->Auth->user('name');
                if ($this->Course->save($this->data)) {
                    $this->Session->setFlash('Session edited', 'flash_success');
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->data = $this->Course->read();
                    $this->Session->setFlash('Some error', 'flash_error');
                }
            }
        }
        
    }
    
    /**
     * delete course
     * 
     * @param int $id
     */
    public function delete($id) {
        $this->Course->delete($id);
        $this->redirect(array('action' => 'index'));        
    }
    
}
?>
