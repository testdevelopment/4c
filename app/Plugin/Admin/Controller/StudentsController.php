<?php
class StudentsController extends AdminAppController {
    var $uses = array('Course', 'Student');
    var $permissions = array(
        'add'     => '*',
        'edit'    => '*',
        'cancel'  => '*',
        'process' => '*',
        'view'    => '*',
        'preEnrolled' => '*',
        'enrolled'    => '*',
        'cancelledRegistrations' => '*',
        'delete'  => array('administrator', 'curator lvl1')
    );
    
    public function add() {
        $this->set('classes', $this->Course->Subject->find('list', array('fields' => array('Subject.name'))));
        
        if($this->request->is('post')) {
            $this->Student->create();
            $this->request->data['Student']['registration_date'] = date('m/d/Y');
            
            if($this->request->data['Student']['enrollment_status'] == 'Enrolled') {
                $this->request->data['Student']['enrolled_date'] = date('m/d/Y');
            }
            
            if($this->Student->save($this->request->data)) {
                if($this->request->data['Student']['enrollment_status'] == 'Enrolled') {
                    $this->Student->Course->updateAll(
                        array('Course.curent_enrolment' => 'Course.curent_enrolment+1'),
                        array('Course.id' => $this->request->data['Student']['course_id'])
                    );
                }
                
                $this->Session->setFlash(__('Student added'), 'flash_success');
                $this->Student->writeHistory('Student added from admin panel', $this->Student->getInsertID(), $this->Auth->user('name'));
                return $this->redirect(array('plugin' => 'admin', 'controller' => 'courses', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('Happened some error. Plaese try again'), 'flash_error');
            }
        }
    }

    public function edit($id) {
        $this->Student->id = $id;

        if (!empty($this->data)) {
            if ($this->Student->save($this->data)) {
                $this->Session->setFlash('Student edited', 'flash_success');
                $this->Student->writeHistory('Student edited from admin panel', $id, $this->Auth->user('name'));
                return $this->redirect(array('plugin' => 'admin', 'controller' => 'courses', 'action' => 'index'));
            } else {
                $this->set('classes', $this->Course->Subject->find('list', array('fields' => array('Subject.name'))));
                $this->Session->setFlash('Some error', 'flash_error');
            }
        } else {
            $this->set('classes', $this->Course->Subject->find('list', array('fields' => array('Subject.name'))));
            $this->data = $this->Student->read();
        }
    }
    
    public function cancel($id, $cancelledEnrolled = NULL) {
        $this->Student->id = $id;
        
        if($cancelledEnrolled != NULL) {
            $student = $this->Student->findById($id, array('course_id'));
            $this->Student->Course->updateAll(
                    array('Course.curent_enrolment' => 'Course.curent_enrolment-1'),
                    array('Course.id' => $student['Student']['course_id'])
            );
        }
        
        $this->Student->saveField('enrollment_status','Cancelled');
        $this->Student->writeHistory('Student cencelled', $id, $this->Auth->user('name'));

        return $this->redirect($this->referer());
    }
    
    public function process($id) {
        $this->Student->id = $id;
        
        $student = $this->Student->findById($id, array('course_id'));
        $this->Student->Course->updateAll(
                array('Course.curent_enrolment' => 'Course.curent_enrolment+1'),
                array('Course.id' => $student['Student']['course_id'])
        );
        
        $this->Student->saveField('enrollment_status','Enrolled');
        $this->Student->saveField('enrolled_date', date('m/d/Y'));
        $this->Student->writeHistory('Student enrolled', $id, $this->Auth->user('name'));
        
        return $this->redirect($this->referer());
    }
    
    public function view($id) {
        $this->loadModel('StudentHistory');
        $this->Student->id = $id;
        
        if($this->request->is('post')) {
            if(!$this->Student->writeHistory($this->request->data['StudentHistory']['text'], $id, $this->Auth->user('name'))) {
                $this->Session->setFlash(__('Item not saved'), 'flash_error');
            }
        }
        
        $this->data = $this->Student->read();
        $student    = $this->Student->find('first', array('conditions' => array('Student.id' => $id), 'recursive' => 2));
        $this->set('student', $student);
    }
    
    public function preEnrolled($id = NULL) {
        if($id == NULL) {
            $students = $this->Student->find('all', array(
                'conditions' => array('Student.enrollment_status' => 'Pre-Enrolled'),
                'fields' => array('Student.*', 'Course.subject_id')
            ));
        } else {
            $students = $this->Student->find('all', array(
                'conditions' => array(
                    'Student.enrollment_status' => 'Pre-Enrolled',
                    'Student.course_id' => $id
                ),
                'fields' => array('Student.*', 'Course.subject_id')
            ));
        }
        
        $students = $this->Student->addSubjectName($students);

        $this->set('students', $students);
    }

    public function enrolled($id) {
        $students = $this->Student->find('all', array(
            'conditions' => array('Student.enrollment_status' => 'Enrolled', 'Student.course_id' => $id)
            ));
        $course = $this->Course->find('first', array('conditions' => array('Course.id' => $id)));
        $this->set('students', $students);
        $this->set('className', $course['Subject']['name']);
    }
    
    public function cancelledRegistrations() {
        $students = $this->Student->find('all', array(
            'conditions' => array('Student.enrollment_status' => 'Cancelled'),
            'recursive'  => 2
        ));
        
        $this->set('students', $students);
    }
    
    public function delete($id) {
        $this->Student->delete($id);
        $this->redirect($this->referer());        
    }
}
