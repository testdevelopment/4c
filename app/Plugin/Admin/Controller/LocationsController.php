<?php

class LocationsController extends AdminAppController {
    
    var $uses = 'Location';
    var $permissions = array(
        'index'  => '*',
        'view'   => '*',
        'add'    => '*',
        'edit'   => '*',
        'delete' => array('administrator', 'curator lvl1')
    );

    /**
     * Index
     *
     * @return void
     */
    public function index() {
        $this->set('locations', $this->Location->find('all'));
    }
    
    public function delete($id) {
        $this->Location->delete($id);
        $this->redirect(array('action' => 'index'));
    }
}
