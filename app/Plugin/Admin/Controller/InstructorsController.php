<?php

class InstructorsController extends AdminAppController {
    
    var $uses = 'Instructor';
    var $permissions = array(
        'index'  => '*',
        'view'   => '*',
        'add'    => '*',
        'edit'   => '*',
        'delete' => array('administrator', 'curator lvl1')
    );

    /**
     * Index
     *
     * @return void
     */
    public function index() {
        $this->set('instructors', $this->Instructor->find('all'));
    }

    public function delete($id) {
        $this->Instructor->delete($id);
        $this->redirect(array('action' => 'index'));
    }

}
