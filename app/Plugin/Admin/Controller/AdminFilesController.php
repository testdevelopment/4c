<?php

class AdminFilesController extends AdminAppController {
    var $uses = 'AdminFile';
    
    var $permissions = array(
        'index'  => '*',
        'edit'   => '*',
        'downloadFile'  => '*',
        'delete' => array('administrator', 'curator lvl1')
    );

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('downloadFile');
    }

    public function index() {
        if (!empty($this->data) && is_uploaded_file($this->data['file']['tmp_name'])) {
            $this->AdminFile->addFile($this->data);
            $this->Session->setFlash('File uploaded', 'flash_success');
        }

        $this->set('files', $this->AdminFile->find('all'));
    }
    
    public function edit($id) {
        $this->AdminFile->id = $id;

        if ($this->request->is('post')) {
            if ($this->AdminFile->save($this->request->data)) {
                $this->Session->setFlash('File edited', 'flash_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Some error', 'flash_error');
            }
        }

        $this->data = $this->AdminFile->read();
    }
    
    public function delete($name, $id) {
        $file_path = Configure::read('download_way');
        
        $file = new File($file_path.$name);
        $file->delete();
        
        $this->AdminFile->delete($id);
        
        $this->redirect(array('action' => 'index'));
    }

    /*
     * download file
     * @return file
     */
    public function downloadFile($name, $title) {
        $file_path = Configure::read('download_way');
        
        $this->response->file($file_path.$name, array(
                'download' => true,
                'name' => $title,
            ));
        return $this->response;
    }
    
}
