<?php
class AdminUsersController extends AdminAppController {
    var $permissions = array(
        'index'  => '*',
        'logout' => '*'
    );

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login');
    }

    var $name = 'AdminUsers';
    var $helpers = array('Form');

    /**
     * show all AdminUsers
     */
    function index() {
        $this->set('adminUsers', $this->AdminUser->find('all'));
    }
    
    /**
     * add AdminUser
     */
    function add() {
        if($this->request->is('post')) {
            $this->AdminUser->create();
            $this->request->data['AdminUser']['password'] = AuthComponent::password($this->data['AdminUser']['password']);
            
            if($this->AdminUser->save($this->request->data)) {
                $this->Session->setFlash(__('User added'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Error. Try again'), 'flash_error');
            }
        }
    }

    /**
     * edit AdminUser (name, email, group)
     */
    function edit($id = null) {
        $this->AdminUser->id = $id;
        if (empty($this->data)) {
            $this->data = $this->AdminUser->read();
        } else {
            if ($this->AdminUser->save($this->data)) {
                $this->Session->setFlash('User edited', 'flash_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->data = $this->AdminUser->read();
                $this->Session->setFlash('Some error', 'flash_error');
            }
        }
    }
    
    /**
     * delete AdminUser
     */
    function delete($id) {
        $this->AdminUser->delete($id);
        $this->redirect(array('action' => 'index'));
    }
    
    /**
     * login page
     */
    function login() {
        if (!empty($this->data)) {
            $user = $this->AdminUser->find(
                'all',
                array(
                    'conditions' => array(
                        'AdminUser.login' => $this->data['AdminUser']['login'],
                        'AdminUser.password' => AuthComponent::password($this->data['AdminUser']['password'])
                    )
                )
            );

            if($user != false) {
                if($this->Auth->login()) {
                    $userGroup = $this->AdminUser->Group->field('name',array('id' => $this->Auth->user('group_id')));
                    $this->Session->write('Auth.User.group', $userGroup);
                    $this ->Session->setFlash('You are logged in', 'flash_info');
                    $this->Redirect(array('controller' =>'admin', 'action' => 'index'));
                } else {
                    $this ->Session->setFlash('Error. Try again', 'flash_error');
                }
            } else {
                $this->Session-> setFlash('Incorrect username/password!', 'flash_error');
            }
        }
    }
    
    /**
     * logaut function. does not have page
     */
    function logout() {
        $this->Session->setFlash('You are logout', 'default', array('class' => 'flash_info'));
        return $this->redirect($this->Auth->logout());
    }
}

?>

