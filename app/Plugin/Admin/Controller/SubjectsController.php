<?php

class SubjectsController extends AdminAppController {
    
    var $uses = 'Subject';
    var $permissions = array(
        'index'  => '*',
        'view'   => '*',
        'add'    => '*',
        'edit'   => '*',
        'delete' => array('administrator', 'curator lvl1')
    );

    /**
     * Index
     *
     * @return void
     */
    public function index() {
        $this->set('subjects', $this->Subject->find('all'));
    }
    
    /**
     * add new subject (class)
     * 
     * @return callable redirect
     */
    public function add() {
        if($this->request->is('post')) {
            $this->Subject->create();
            if($this->Subject->save($this->request->data)) {
                $this->Session->setFlash(__('Class added'), 'default', array('class' => 'flash_success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Error. Try again'), 'default', array('class' => 'flash_error'));
            }
        }
    }
    
    public function delete($id) {
        $this->Subject->delete($id);
        $this->redirect(array('action' => 'index'));
    }
}
?>
