<?php
class AdminUser extends AdminAppModel {
    var $name = 'AdminUser';
    var $belongsTo = array('Group');
    
    public $validate = array(
        'name'  => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'login' => array(
            'notEmpty' => array(
                'rule'     => 'notEmpty',
                'required' => true,
                'on'      => 'create'
            ),
            'loginUnique' => array(
                'rule'    => 'isUnique',
                'message' => 'This login is occupied'
            ),
        ),
        'email' => array(
            'notEmpty' => array(
                'rule'     => 'notEmpty',
                'required' => true
            ),
            'email' => array(
                'rule'    => 'email',
                'message' => 'Inpute valid email'
            )
        )
    );
}
?>
