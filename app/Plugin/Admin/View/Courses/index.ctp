<h3>Sessions</h3>

<p><?php
    echo $this->Html->link(
        'Add New Session',
        array('controller' => 'courses', 'action' => 'add', 'plugin' => 'admin'),
        array('class' => 'btn btn-info')
    );
    echo $this->Html->link(
        'Show active Sessions',
        array('controller' => 'courses', 'action' => 'index', 'Active', 'plugin' => 'admin'),
        array('class' => 'btn btn-info')
    );
    echo $this->Html->link(
        'Show inactive Sessions',
        array('controller' => 'courses', 'action' => 'index', 'Inactive', 'plugin' => 'admin'),
        array('class' => 'btn btn-info')
    );
    echo $this->Html->link(
        'Show All Sessions',
        array('controller' => 'courses', 'action' => 'index', 'plugin' => 'admin'),
        array('class' => 'btn btn-info')
    );
?></p>


<table class="table table-striped" >
    <tr>
        <th>ID</th>
        <th>Edit</th>
        <th>Name</th>
        <th>Enrolled</th>
        <th>Pre-Enrolled</th>
        <th>Start date</th>
        <th>Location</th>
        <th>Active</th>
        <th>Delete</th>
    </tr>
    <?php
        foreach ($courses as $course):
            $preEnrolledCoutn = 0;
            $preEnrolledClass = 'text-muted';
            foreach ($course['Student'] as $student) {
                if($student['enrollment_status'] == 'Pre-Enrolled') {
                    ++$preEnrolledCoutn;
                    $preEnrolledClass = 'text-success';
                }
            }
    ?>
    <tr>
        <td><?php echo $course['Course']['id']; ?></td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-edit"></span>',
                array(
                    'controller' => 'courses',
                    'action' => 'edit',
                    $course['Course']['id'],
                    'plugin' => 'admin'
                ),
                array(
                    'title' => 'Edit',
                    'escape'=>false
                )
            ); ?>
        </td>
        <td><?php echo $this->Html->link(
                $course['Subject']['name'],
                array(
                    'controller' => 'courses',
                    'action' => 'view',
                    $course['Course']['id'],
                    'plugin' => 'admin'
                ),
                array('title' => 'view Session')
            ); ?>
        </td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-user"></span>',
                array(
                    'controller' => 'students',
                    'action' => 'enrolled',
                    $course['Course']['id'],
                    'plugin' => 'admin'
                ),
                array('escape'=>false)
            ); ?>
        </td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-user '.$preEnrolledClass.'"></span>',
                array(
                    'controller' => 'students',
                    'action' => 'preEnrolled',
                    $course['Course']['id'],
                    'plugin' => 'admin'
                ),
                array('escape'=>false, 'title' => $preEnrolledCoutn)
            ); ?>
        </td>
        <td class="td-horisontal-center"><?php echo $course['Course']['start_date']; ?></td>
        <td><?php echo $course['Location']['name']; ?></td>
        <td class="td-horisontal-center"><?php echo $course['Course']['active']; ?></td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-remove-circle"></span>',
                array(
                    'controller' => 'courses',
                    'action' => 'delete',
                    $course['Course']['id'],
                    'plugin' => 'admin'
                ),
                array(
                    'title' => 'Delete',
                    'escape'=>false
                ),
                "Are you sure you wish to delete this Session?"
            ); ?>
        </td>
    </tr>
    <?php endforeach; /* courses as course */ ?>
</table>
