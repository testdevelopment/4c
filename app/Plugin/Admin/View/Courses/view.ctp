<h3>
    <a href="javascript: history.back();" class="btn btn-default btn-sm">
        <span class="glyphicon glyphicon-circle-arrow-left"></span>
        Go back
    </a>
    View session id <?php echo $course['Course']['id']; ?>
</h3>
<table class="table table-striped" id="view-session-table" >
    <tr>
        <td>Class</td>
        <td><?php echo $course['Subject']['name']; ?></td>
    </tr>
    <tr>
        <td>Start date</td>
        <td><?php echo $course['Course']['start_date']; ?></td>
    </tr>
    <tr>
        <td>End date</td>
        <td><?php echo $course['Course']['end_date']; ?></td>
    </tr>
    <tr>
        <td>Number of sessions</td>
        <td><?php echo $course['Course']['number_of_sessions']; ?></td>
    </tr>
    <tr>
        <td>Class time</td>
        <td><?php echo $course['Course']['class_time']; ?></td>
    </tr>
    <tr>
        <td>Confirmation date</td>
        <td><?php echo $course['Course']['confirmation_date']; ?></td>
    </tr>
    <tr>
        <td>Maximum enrolment</td>
        <td><?php echo $course['Course']['maximum_enrolment']; ?></td>
    </tr>
    <tr>
        <td>Curent enrolment</td>
        <td><?php echo $course['Course']['curent_enrolment']; ?></td>
    </tr>
    <tr>
        <td>Instructor 1</td>
        <td><?php echo $course['Instructor1']['name']; ?></td>
    </tr>
    <tr>
        <td>Instructor 2</td>
        <td><?php echo $course['Instructor2']['name']; ?></td>
    </tr>
    <tr>
        <td>Location</td>
        <td><?php echo $course['Location']['name']; ?></td>
    </tr>
    <tr>
        <td>Confirmation instructions</td>
        <td><?php echo $course['Course']['confirmation_instructions']; ?></td>
    </tr>
    <tr>
        <td>Semester</td>
        <td><?php echo $course['Course']['semester']; ?></td>
    </tr>
    <tr>
        <td>Year</td>
        <td><?php echo $course['Course']['year']; ?></td>
    </tr>
    <tr>
        <td>Free</td>
        <td><?php echo $course['Course']['free']; ?></td>
    </tr>
    <tr>
        <td>Active</td>
        <td><?php echo $course['Course']['active']; ?></td>
    </tr>
    <tr>
        <td>Modified by</td>
        <td><?php echo $course['Course']['modified_by']; ?></td>
    </tr>
    <tr>
        <td>Modified</td>
        <td><?php echo $course['Course']['modified']; ?></td>
    </tr>
</table>
