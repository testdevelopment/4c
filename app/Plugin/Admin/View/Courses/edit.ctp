<h3>
    <a href="javascript: history.back();" class="btn btn-default btn-sm">
        <span class="glyphicon glyphicon-circle-arrow-left"></span>
        Go back
    </a>
    Edit Session
</h3>

<?php
echo $this->Form->create('Course', array('class' => 'form-horizontal'));
echo $this->form->input('Course.subject_id', array(
        'options' => $selectOptions['subjects'],
        'label'   => array(
            'text'  => 'Class',
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->Form->input('Course.start_date', array(
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->Form->input('Course.end_date', array(
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->Form->input('Course.number_of_sessions', array(
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->Form->input('Course.class_time', array(
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->Form->input('Course.confirmation_date', array(
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'class' => 'form-control',
        'div' => 'form-group',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->Form->input('Course.maximum_enrolment', array(
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
?>
<div class="form-group">
    <label class="col-sm-2 control-label">Curent enrolment</label>
    <div class="col-sm-4"><?php echo $this->request->data['Course']['curent_enrolment']; ?></div>
</div>
<?php
echo $this->form->input('Course.instructor_1_id', array(
        'options' => $selectOptions['instructors'],
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->form->input('Course.instructor_2_id', array(
        'options' => $selectOptions['instructors'],
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->form->input('Course.location_id', array(
        'options' => $selectOptions['locations'],
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->Form->input('Course.confirmation_instructions', array(
        'type' => 'textarea',
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->form->input('Course.semester', array(
        'options' => array(
            '1st Quarter' => '1st Quarter',
            '2nd Quarter' => '2nd Quarter',
            '3rd Quarter' => '3rd Quarter',
            '4th Quarter' => '4th Quarter',
            '1st Half' => '1st Half',
            '2nd Half' => '2nd Half'
        ),
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->Form->input('Course.year', array(
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->Form->input('Course.free', array(
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);
echo $this->form->input('Course.active', array(
        'options' => array(
            'Active' => 'Yes',
            'Inactive' => 'No'
        ),
        'label'   => array(
            'class' => 'col-sm-2 control-label'
        ),
        'div' => 'form-group',
        'class' => 'form-control',
        'between' => '<div class="col-sm-4">',
        'after' => '</div>'
    )
);


echo $this->Form->submit('Edit', array('class' => 'btn btn-success save-post'));
echo $this->Form->end();
?>
<script>
 $(function() {
    $("#CourseStartDate").datepicker();
    $("#CourseEndDate").datepicker();
    $("#CourseConfirmationDate").datepicker();
});
</script>
