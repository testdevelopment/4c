<h3>Edit administrator user</h3>
<?php
echo $this->form->create('AdminUser', array('action' => 'edit'));
echo $this->form->input('name');
echo $this->form->input('email');
echo $this->form->input(
    'group_id',
    array(
        'type' => 'select',
        'options' => array(1 => 'administrator', 2 => 'curator lvl1', 3 => 'curator lvl2')
    )
);
echo $this->Form->submit('Save', array('class' => 'btn btn-success save-post'));
echo $this->form->end();
?>
