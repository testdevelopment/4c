<h3>Administrators</h3>

<p><?php echo $this->Html->link(
        'Add admin',
        array('controller' => 'admin_users', 'action' => 'add', 'plugin' => 'admin'),
        array('class' => 'btn btn-info')
    );
?></p>

<table class="table table-bordered table-striped">
    <tr>
        <th>Edit</th>
        <th>Name</th>
        <th>Group</th>
        <th>email</th>
        <th>Delete</th>
    </tr>
    <?php foreach ($adminUsers as $user): ?>
    <tr>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-edit"></span>',
                array(
                    'controller' => 'admin_users',
                    'action' => 'edit',
                    $user['AdminUser']['id'],
                    'plugin' => 'admin'
                ),
                array('escape'=>false)
            ); ?>
        </td>
        <td><?php echo $user['AdminUser']['name']; ?></td>
        <td><?php echo $user['Group']['name']; ?></td>
        <td><?php echo $user['AdminUser']['email']; ?></td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-remove-circle"></span>',
                array(
                    'controller' => 'admin_users',
                    'action' => 'delete',
                    $user['AdminUser']['id'],
                    'plugin' => 'admin'
                ),
                array('escape'=>false),
                "Are you sure you wish to delete this user?"
            ); ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
