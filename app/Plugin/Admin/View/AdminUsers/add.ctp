<h3>Addin new AdminUser</h3>

<?php
echo $this->form->create('AdminUser');
echo $this->form->input('name', array('autofocus'=>'autofocus'));
echo $this->form->input('login');
echo $this->form->input('password');
echo $this->form->input('email');
echo $this->form->input(
    'group_id',
    array(
        'type' => 'select',
        'options' => array(1 => 'administrator', 2 => 'curator lvl1', 3 => 'curator lvl2'),
        'selected' => 2
    )
);
echo $this->Form->submit('Save', array('class' => 'btn btn-success save-post'));
echo $this->form->end();
?>
