<h3>Locations</h3>

<p><?php echo $this->Html->link(
        'Add Location',
        array('controller' => 'locations', 'action' => 'add', 'plugin' => 'admin'),
        array('class' => 'btn btn-info')
    );
?></p>

<table class="table table-bordered table-striped" cellspacing="0">
    <tr>
        <th>Name</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <?php foreach ($locations as $location): ?>
    <tr>
        <td><?php echo $this->Html->link(
                $location['Location']['name'],
                array(
                    'controller' => 'locations',
                    'action' => 'view',
                    $location['Location']['id'],
                    'plugin' => 'admin'
                ),
                array('title' => 'view')
            ); ?>
        </td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-edit"></span>',
                array(
                    'controller' => 'locations',
                    'action' => 'edit',
                    $location['Location']['id'],
                    'plugin' => 'admin'
                ),
                array('escape'=>false)
            ); ?>
        </td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-remove-circle"></span>',
                array(
                    'controller' => 'locations',
                    'action' => 'delete',
                    $location['Location']['id'],
                    'plugin' => 'admin'
                ),
                array('escape'=>false),
                "Are you sure you wish to delete location ".$location['Location']['name']."?"
            ); ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
