<h3>Classes</h3>

<p><?php echo $this->Html->link(
        'Add Class',
        array('controller' => 'subjects', 'action' => 'add', 'plugin' => 'admin'),
        array('class' => 'btn btn-info')
    );
?></p>

<table class="table table-bordered table-striped" cellspacing="0">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <?php foreach ($subjects as $subject): ?>
    <tr>
        <td class="td-horisontal-center"><?php echo $subject['Subject']['id']; ?></td>
        <td><?php echo $this->Html->link(
                $subject['Subject']['name'],
                array(
                    'controller' => 'subjects',
                    'action' => 'view',
                    $subject['Subject']['id'],
                    'plugin' => 'admin'
                ),
                array('title' => 'view class')
            ); ?>
        </td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-edit"></span>',
                array(
                    'controller' => 'subjects',
                    'action' => 'edit',
                    $subject['Subject']['id'],
                    'plugin' => 'admin'
                ),
                array('escape'=>false)
            ); ?>
        </td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-remove-circle"></span>',
                array(
                    'controller' => 'subjects',
                    'action' => 'delete',
                    $subject['Subject']['id'],
                    'plugin' => 'admin'
                ),
                array('escape'=>false),
                "Are you sure you wish to delete class ".$subject['Subject']['name']."?"
            ); ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
