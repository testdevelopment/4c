<h3>Add new Class</h3>

<?php
echo $this->Form->create('Subject');
echo $this->Form->input('name', array('autofocus'=>'autofocus'));
echo $this->Form->input('description',
    array('type' => 'textarea',
        'class' => 'form-control',
        'placeholder' => 'Max. 1000 chars'
    )
);
echo $this->Form->submit('Save', array('class' => 'btn btn-success save-post'));
echo $this->form->end();
?>
