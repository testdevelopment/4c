<h3>Edit file</h3>
<?php
echo $this->Form->create('AdminFile', array('action' => 'edit'));
echo $this->Form->input('AdminFile.title');
echo $this->Form->input(
    'AdminFile.status',
    array(
        'type'    => 'select',
        'options' => array('Show' => 'Show', 'Hide' => 'Hide')
    )
);

echo $this->Form->submit('Save', array('class' => 'btn btn-success save-post'));
echo $this->Form->end();
?>

