<h3>Files</h3>

<p>
    <?php
    echo $this->Form->create('AdminFile', array('enctype' => 'multipart/form-data'));
    echo $this->form->file('file');
    echo $this->form->input('title');

    echo $this->Form->submit('Add', array('class' => 'btn btn-success save-post'));
    echo $this->Form->end();
    ?>
</p>

<table class="table table-bordered table-striped" cellspacing="0">
    <tr>
        <th>Edit</th>
        <th>Name</th>
        <th>Status</th>
        <th>Delete</th>
    </tr>
    <?php foreach ($files as $file): ?>
    <tr>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-edit"></span>',
                array(
                    'controller' => 'admin_files',
                    'action' => 'edit',
                    $file['AdminFile']['id'],
                    'plugin' => 'admin'
                ),
                array(
                    'title' => 'Edit',
                    'escape'=>false
                )
            ); ?>
        </td>
        <td><?php echo $this->Html->link(
                $file['AdminFile']['title'],
                array(
                    'controller' => 'admin_files',
                    'action' => 'downloadFile',
                    $file['AdminFile']['name'], $file['AdminFile']['title'],
                    'plugin' => 'admin'
                ),
                array('title' => $file['AdminFile']['name'])
            ); ?>
        </td>
        <td class="td-horisontal-center"><?php echo $file['AdminFile']['status'];?></td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-remove-circle"></span>',
                array(
                    'controller' => 'admin_files',
                    'action' => 'delete',
                    $file['AdminFile']['name'],
                    $file['AdminFile']['id'],
                    'plugin' => 'admin'
                ),
                array(
                    'title' => 'Delete',
                    'escape'=>false
                ),
                "Are you sure you wish to delete this File?"
            ); ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
