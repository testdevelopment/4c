<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Admin</title>
<!--[if lt IE 9] >
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php echo $this->Html->css('Admin.bootstrap'); ?>
<?php echo $this->Html->css('Admin.jquery-ui'); ?>
<?php echo $this->Html->css('Admin.style'); ?>

<?php echo $this->Html->script('Admin.jquery-1.11.1.min'); ?>
<?php echo $this->Html->script('Admin.jquery-ui.min'); ?>
<?php echo $this->Html->script('Admin.bootstrap.min'); ?>

</head>
<body>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <?php
            if($this->Session->check('Auth.User')) {
                echo $this->element('menu');
            }
            ?>
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->Session->flash('auth'); ?>
            <?php echo $this->fetch('content'); ?>
        </div>
        <div class="col-md-1"></div>
    </div>
    <hr>
    <div class="row-fluid">
        <?php echo str_replace('class="cake-sql-log"', 'class="table table-bordered table-striped"', $this->element('sql_dump')); ?>
    </div>
</body>
</html>
