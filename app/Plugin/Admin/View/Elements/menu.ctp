<div class="admin-menu">
    <ul class="nav nav-pills">
        <li><?php echo $this->Html->link('Main', array('plugin' => 'admin', 'controller' => 'admin', 'action' => 'index')); ?></li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              Session Administration <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><?php echo $this->Html->link('Sessions', array('plugin' => 'admin', 'controller' => 'courses', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Pre-enrolled Queue <span class="badge">'.$preEnrolls.'</span>', array('plugin' => 'admin', 'controller' => 'students', 'action' => 'preEnrolled'), array('escape' => FALSE)); ?></li>
                <li><?php echo $this->Html->link('Cancelled Registrations', array('plugin' => 'admin', 'controller' => 'students', 'action' => 'cancelledRegistrations')); ?></li>
                <li><?php echo $this->Html->link('Add new student', array('plugin' => 'admin', 'controller' => 'students', 'action' => 'add')); ?></li>
            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                Session's elements <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><?php echo $this->Html->link('Classes', array('plugin' => 'admin', 'controller' => 'subjects', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Instructors', array('plugin' => 'admin', 'controller' => 'instructors', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Locations', array('plugin' => 'admin', 'controller' => 'locations', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Documents', array('plugin' => 'admin', 'controller' => 'admin_files', 'action' => 'index')); ?></li>
            </ul>
        </li>
        <li><?php echo $this->Html->link('Admin Users', array('plugin' => 'admin', 'controller' => 'admin_users', 'action' => 'index')); ?></li>
        <li><?php echo $this->Html->link('Logout', array('plugin' => 'admin', 'controller' => 'admin_users', 'action' => 'logout')); ?></li>
        <li><?php echo $this->Html->link('Vist site', array('plugin' => false, 'controller' => 'pages', 'action' => 'home'), array('target' => 'blank')); ?></li>
    </ul>
</div>
