<h3>Cancelled Registrations</h3>

<table class="table table-striped">
    <th>ID</th>
    <th>Process</th>
    <th>Name</th>
    <th>Session ID</th>
    <th>Session Name</th>
    <th>Date submited</th>
    <th>Delete</th>
    <?php foreach ($students as $student) : ?>
        <tr>
            <td class="td-horisontal-center"><?php echo $student['Student']['id']; ?></td>
            <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-ok-circle"></span>',
                array(
                    'controller' => 'students',
                    'action' => 'process',
                    $student['Student']['id'],
                    'plugin' => 'admin'
                ),
                array(
                    'title' => 'Enroll',
                    'escape'=>false
                )
            ); ?></td>
            <td><?php echo $this->Html->link(
                    $student['Student']['first_name'].' '.$student['Student']['last_name'],
                    array('plugin' => 'admin', 'controller' => 'students', 'action' => 'view', $student['Student']['id'])
                    ); ?></td>
            <td class="td-horisontal-center"><?php echo $student['Student']['course_id']; ?></td>
            <td class="td-horisontal-center"><?php echo $student['Course']['Subject']['name']; ?></td>
            <td class="td-horisontal-center"><?php echo $student['Student']['registration_date']; ?></td>
            <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-remove-circle"></span>',
                array(
                    'controller' => 'students',
                    'action' => 'delete',
                    $student['Student']['id'],
                    'plugin' => 'admin'
                ),
                array(
                    'title' => 'Delete',
                    'escape'=>false
                )
            ); ?></td>
        </tr>
    <?php endforeach; ?>
</table>

