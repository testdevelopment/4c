<h3>
    <a href="javascript: history.back();" class="btn btn-default btn-sm">
        <span class="glyphicon glyphicon-circle-arrow-left"></span>
        Go back
    </a>
    Edit Student
</h3>

<?php
echo $this->Form->create('Student');
echo $this->Form->input('Student.first_name');
echo $this->Form->input('Student.last_name');
echo $this->Form->input('Student.social_security_number');
echo $this->Form->input('Student.address');
echo $this->Form->input('Student.city');
echo $this->Form->input('Student.country');
echo $this->Form->input('Student.state');
echo $this->Form->input('Student.zip');
echo $this->Form->input('Student.home_phone', array('required' => false));
echo $this->Form->input('Student.alt_phone', array('required' => false));
echo $this->Form->input('Student.email', array('required' => false));
echo $this->Form->input(
    'Student.18_age',
    array(
        'type' => 'radio',
        'options' => array(1 => 'Yes', 0 => 'No'),
        'legend' => '18 years of age or older'
    )
);
echo $this->Form->input(
    'Student.enrollment_status',
    array('options' => array(
        'Pre-Enrolled' => 'Pre-Enrolled',
        'Enrolled' => 'Enrolled',
        'Cancelled' => 'Cancelled'),
    )
);
echo $this->Form->input('Student.course_id', array('options' => $classes));
echo $this->Form->input('Student.amount_paid');
echo $this->Form->input('Student.paid_date');

echo $this->Form->submit('Save', array('class' => 'btn btn-success save-post'));
echo $this->form->end();
?>
<script>
 $(function() {
    $("#StudentPaidDate").datepicker();
});
</script>
