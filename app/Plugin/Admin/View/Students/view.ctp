<div class="row">
    <div class="col-sm-6">
        <h3>Registration status</h3>
        <table class="table width-auto">
            <tr>
                <td class="text-muted">Enrollment status</td>
                <td><?php echo $student['Student']['enrollment_status']; ?></td>
            </tr>
            <tr>
                <td class="text-muted">Date Reg Submited</td>
                <td><?php echo $student['Student']['registration_date']; ?></td>
            </tr>
            <tr>
                <td class="text-muted">Date Enrolled</td>
                <td><?php echo $student['Student']['enrolled_date']; ?></td>
            </tr>
            <tr>
                <td class="text-muted">Amount Paid</td>
                <td><?php echo $student['Student']['amount_paid']; ?></td>
            </tr>
            <tr>
                <td class="text-muted">Date Paid</td>
                <td><?php echo $student['Student']['paid_date']; ?></td>
            </tr>
            <tr>
                <td class="text-muted">Transaction ID</td>
                <td><?php echo $student['Student']['transaction_id']; ?></td>
            </tr>
        </table>
        
        <h3>History</h3>
        <?php
        echo $this->Form->create('StudentHistory');
        echo $this->Form->textarea('StudentHistory.text', array('class' => 'form-control'));

        echo $this->Form->submit('Add', array('class' => 'btn btn-success save-post'));
        echo $this->Form->end();
        ?>
        <table class="table table-bordered" id="student-history-table">
            <tr>
                <th class="date">Date</th>
                <th class="col-sm-2">Modified by</th>
                <th>Description</th>
            </tr>
            <?php
            foreach ($student['StudentHistory'] as $history) : ?>
                <tr>
                    <td><?php echo $history['modified']; ?></td>
                    <td><?php echo $history['modified_by']; ?></td>
                    <td><?php echo $history['text']; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    
    <div class="col-sm-6">
        <h3>Class information</h3>
        <table class="table width-auto">
            <tr>
                <td class="text-muted">ClassName</td>
                <td><?php echo $student['Course']['Subject']['name']; ?></td>
            </tr>
            <tr>
                <td class="text-muted">Start Date</td>
                <td><?php echo $student['Course']['start_date']; ?></td>
            </tr>
            <tr>
                <td class="text-muted">End Date</td>
                <td><?php echo $student['Course']['end_date']; ?></td>
            </tr>
            <tr>
                <td class="text-muted">Class Time</td>
                <td><?php echo $student['Course']['class_time']; ?></td>
            </tr>
            <tr>
                <td class="text-muted">Location</td>
                <td><?php echo $student['Course']['Location']['name']; ?></td>
            </tr>
            <tr>
                <td class="text-muted">Free</td>
                <td><?php echo $student['Course']['free']; ?></td>
            </tr>
        </table>

        <h3>Registration form</h3>
        <?php
        echo $this->Form->create('Student');
        echo $this->Form->input('first_name', array('readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input('last_name', array('readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input('social_security_number', array('readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input('address', array('readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input('city', array('readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input('country', array('readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input('state', array('readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input('zip', array('readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input('home_phone', array('required' => false, 'readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input('alt_phone', array('required' => false, 'readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input('email', array('required' => false, 'readonly', 'div' => array('class' => 'input text text-muted')));
        echo $this->Form->input(
            '18_age',
            array(
                'type'    => 'radio',
                'options' => array(1 => 'Yes', 0 => 'No'),
                'legend'  => '18 years of age or older',
                'class'   => 'text-muted',
                'div'     => array('class' => 'input radio text-muted'),
                'readonly'
            )
        );
        echo $this->form->end();
        ?>
    </div>
</div>
