<h3>Enrolled students</h3>
<h3>Class: <?php echo $className;?></h3>
<table class="table table-striped">
    <th>ID</th>
    <th>Edit</th>
    <th>Name</th>
    <th>Date submited</th>
    <th>Date enrolled</th>
    <th>Cancle</th>
    <?php foreach ($students as $student) : ?>
        <tr>
            <td class="td-horisontal-center"><?php echo $student['Student']['id']; ?></td>
            <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-edit"></span>',
                array(
                    'controller' => 'students',
                    'action' => 'edit',
                    $student['Student']['id'],
                    'plugin' => 'admin'
                ),
                array(
                    'title' => 'Edit',
                    'escape'=>false
                )
            ); ?></td>
            <td><?php echo $this->Html->link(
                    $student['Student']['first_name'].' '.$student['Student']['last_name'],
                    array('plugin' => 'admin', 'controller' => 'students', 'action' => 'view', $student['Student']['id'])
                    ); ?></td>
            <td class="td-horisontal-center"><?php echo $student['Student']['registration_date']; ?></td>
            <td class="td-horisontal-center"><?php echo $student['Student']['enrolled_date']; ?></td>
            <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-trash"></span>',
                array(
                    'controller' => 'students',
                    'action' => 'cancel',
                    $student['Student']['id'],
                    true,
                    'plugin' => 'admin'
                ),
                array(
                    'title' => 'Cancle',
                    'escape'=>false
                )
            ); ?></td>
        </tr>
    <?php endforeach; ?>
</table>
