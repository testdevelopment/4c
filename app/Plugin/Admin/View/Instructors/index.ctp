<h3>Instructors</h3>

<p><?php echo $this->Html->link(
        'Add Instructor',
        array('controller' => 'instructors', 'action' => 'add', 'plugin' => 'admin'),
        array('class' => 'btn btn-info')
    );
?></p>

<table class="table table-bordered table-striped" cellspacing="0">
    <tr>
        <th>Name</th>
        <th>Edite</th>
        <th>Delete</th>
    </tr>
    <?php foreach ($instructors as $instructor): ?>
    <tr>
        <td><?php echo $this->Html->link(
                $instructor['Instructor']['name'],
                array(
                    'controller' => 'instructors',
                    'action' => 'view',
                    $instructor['Instructor']['id'],
                    'plugin' => 'admin'
                ),
                array('title' => 'view')
            ); ?>
        </td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-edit"></span>',
                array(
                    'controller' => 'instructors',
                    'action' => 'edit',
                    $instructor['Instructor']['id'],
                    'plugin' => 'admin'
                ),
                array('escape'=>false)
            ); ?>
        </td>
        <td class="td-horisontal-center"><?php echo $this->Html->link(
                '<span class="glyphicon glyphicon-remove-circle"></span>',
                array(
                    'controller' => 'instructors',
                    'action' => 'delete',
                    $instructor['Instructor']['id'],
                    'plugin' => 'admin'
                ),
                array('escape'=>false),
                "Are you sure you wish to delete instructor ".$instructor['Instructor']['name']."?"
            ); ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>

