<?php
class CoursesController extends AppController {

    public function index() {
        $courses = $this->Course->find(
            'all',
            array(
                'conditions' => array(
                    'Course.active' => 'Active',
                    'Course.curent_enrolment < Course.maximum_enrolment'
                )
            )
        );
        
        $this->set('courses', $courses);
    }
    
    public function view($id = NULL) {
        $course = $this->Course->find('first', array('conditions' => array('Course.id' => $id)));
        $this->set('course', $course);
    }
    
    public function registrations($id = NULL) {
        $this->Session->write('Course.id', $id);
    }
    
    public function onlineRegistration() {
        $courseId = $this->Session->read('Course.id');
        
        if($courseId == NULL) {
            $this->Session->setFlash(__('Session was not defined. Choose the Session'), 'flash_error');
            $this->redirect(array('action' => 'index'));
        } else {
            if($this->request->is('post')) {
                $this->Course->Student->create();
                $this->request->data['Student']['enrollment_status'] = 'Pre-Enrolled';
                $this->request->data['Student']['registration_date'] = date('m/d/Y');
                $this->request->data['Student']['course_id'] = $courseId;

                if($this->Course->Student->save($this->request->data)) {
                    $this->Session->write('Student.id', $this->Course->Student->getInsertID());
                    $this->Course->Student->writeHistory('Pre-enrollment submited from website', $this->Course->Student->getInsertID());
                    $item = $this->Course->find('first', array(
                        'fields'     => array('subject_id', 'free'),
                        'conditions' => array('Course.id' => $courseId),
                        'recursive'  => 2
                    ));
                    $this->Session->write('free', $item['Course']['free']);
                    $this->Session->write('name', $item['Subject']['name']);
                    return $this->redirect(array('controller' => 'pages','action' => 'after_pre_registration'));
                } else {
                    $this->Session->setFlash(__('Happened some error. Plaese try again'), 'flash_error');
                    $course = $this->Course->find('first', array('conditions' => array('Course.id' => $courseId)));
                    $this->set('course', $course);
                }
            } else {
                $course = $this->Course->find('first', array('conditions' => array('Course.id' => $courseId)));
                $this->set('course', $course);
            }
        }
    }
    
    public function contactRegistration() {
        $courseId = $this->Session->read('Course.id');

        if($courseId == NULL) {
            $this->Session->setFlash(__('Session was not defined. Choose the Session'), 'flash_error');
            $this->redirect(array('action' => 'index'));
        } else {
            if($this->request->is('post')) {
                $this->Course->Student->set( $this->data );
                if ($this->Course->Student->validates()) {
                    $values   = $this->Course->prepareDataForEmail($this->request->data, $courseId);
                    $emailsTo = $this->Course->getEmailsTo();
                    
                    // sending email
                    App::uses('CakeEmail', 'Network/Email');
                    $Email = new CakeEmail();
                    $Email->template('contact_registration')
                        ->emailFormat('both')
                        ->viewVars($values)
                        ->from(array('orthener@localhost.net' => 'My Site'))
                        ->to($emailsTo)
                        ->subject('New contact registration');

                    if($Email->send()) {
                        return $this->redirect(array('controller' => 'pages','action' => 'after_contact_registration'));
                    } else {
                        $this->Session->setFlash(__('Happened some error. Plaese try again or choose another way for registration'), 'flash_error');
                        $course = $this->Course->find('first', array('conditions' => array('Course.id' => $courseId)));
                        $this->set('course', $course);
                    }
                } else {
                    $this->Session->setFlash(__('Happened some error. Plaese try again'), 'flash_error');
                    $course = $this->Course->find('first', array('conditions' => array('Course.id' => $courseId)));
                    $this->set('course', $course);
                }
            } else {
                $course = $this->Course->find('first', array('conditions' => array('Course.id' => $courseId)));
                $this->set('course', $course);
            }
        }        
    }
    
    public function payment() {
        $studentId = $this->Session->read('Student.id');
        if($courseId == NULL) {
            $this->redirect(array('controller' => 'pages', 'action' => 'student_id_undefined'));
        } else {
            
        }
    }

    public function fileList() {
        $files = ClassRegistry::init('AdminFile')->find('all', array('conditions' => array('AdminFile.status' => 'Show')));
        $this->set('files', $files);
    }
}