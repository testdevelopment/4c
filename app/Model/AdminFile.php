<?php
class AdminFile extends AppModel {

    function addFile ($data) {
        $file_path = Configure::read('download_way');

        $name      = $data['file']['name'];
        $name_name = Inflector::slug(mb_strrchr($name, '.', true));
        $name_type = mb_strrchr($name, '.');
        $name      = $name_name . $name_type;

        $name = $this->CheckName($name);

        move_uploaded_file($data['file']['tmp_name'], $file_path.$name);
        
        $data['name']   = $name;
        $data['status'] = 'Show';
        
        if(empty($data['title'])) {
            $data['title'] = $name_name;
        }
        
        $this->save($data);
    }

    private function CheckName($name){
        if($this->findByName($name)) {
            $point = strripos($name, '.');
            $name = uniqid().substr($name,$point);
            return $this->CheckName($name);
        } else {
            return $name;
        }
    }

}
