<?php
class Student extends AppModel {
    public $belongsTo = array(
        'Course' => array(
            'className'  => 'Course',
            'foreignKey' => 'course_id'
        )
    );    
    public $hasMany = array('StudentHistory');
    
    public $validate = array(
        'first_name' => array(
            'rule'     => 'notEmpty',
            'required' => true
        ),
        'last_name' => array(
            'rule'     => 'notEmpty',
            'required' => true
        ),
        /*
        'social_security_number' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'address' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'city' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        'country' => array(
            'rule' => 'notEmpty',
            'required' => true
        ),
        */
        'home_phone' => array(
                'rule'    => array('leastOneContact'),
                'message' => 'At least one phone number or email is required.'
        ),
        'alt_phone' => array(
                'rule'    => array('leastOneContact'),
                'message' => 'At least one phone number or email is required.'
        ),
        'email' => array(
                'rule'    => array('leastOneContact', 'email'),
                'message' => 'At least one phone number or email is required.'
        )
    );
    
    public function leastOneContact() {
        if(!empty($this->data[$this->name]['home_phone']) || !empty($this->data[$this->name]['alt_phone']) || !empty($this->data[$this->name]['email'])){
            return true;
        } else {
            return false;
        }
    }
    
    public function writeHistory($text, $studentId, $modifiedBy = NULL) {
        $studentHistory['StudentHistory']['text'] = $text;
        $studentHistory['StudentHistory']['student_id'] = $studentId;
        
        if($modifiedBy != NULL) {
            $studentHistory['StudentHistory']['modified_by'] = $modifiedBy;
        }
        
        if($this->StudentHistory->save($studentHistory)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function addSubjectName($students) {
        $studentsOut =array();
        foreach ($students as $student) {
            $subject = $this->Course->Subject->findById($student['Course']['subject_id'], array('name'));
            $student['Course']['subject'] = $subject['Subject']['name'];
            $studentsOut[] = $student;
        }
        
        return $studentsOut;
    }
}