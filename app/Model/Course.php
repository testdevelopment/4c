<?php
class Course extends AppModel {
    public $hasMany = array('Student');
    
    public $belongsTo = array(
        'Subject',
        'Location',
        'Instructor1' => array(
            'className' => 'Instructor',
            'foreignKey' => 'instructor_1_id'
        ),
        'Instructor2' => array(
            'className' => 'Instructor',
            'foreignKey' => 'instructor_2_id'
        )
    );

    public $validate = array(
        'start_date' => array(
            'notEmpty' => array(
                'rule'     => 'notEmpty',
                'required' => true
            ),
            'startDateFormat' => array(
                'rule'    => '#[0-9]{2}/[0-9]{2}/[0-9]{4}#',
                'message' => 'Wrong date format. Use mm/dd/YYYY'
            )
        ),
        'end_date' => array(
            'notEmpty' => array(
                'rule'     => 'notEmpty',
                'required' => true
            ),
            'endDateFormat' => array(
                'rule'    => '#[0-9]{2}/[0-9]{2}/[0-9]{4}#',
                'message' => 'Wrong date format. Use mm/dd/YYYY'
            )
        ),
        'confirmation_date' => array(
            'notEmpty' => array(
                'rule'     => 'notEmpty',
                'required' => true
            ),
            'confirmationDateFormat' => array(
                'rule'    => '#[0-9]{2}/[0-9]{2}/[0-9]{4}#',
                'message' => 'Wrong date format. Use mm/dd/YYYY'
            )
        ),
        'number_of_sessions' => array(
            'rule'     => 'notEmpty',
            'required' => true
        ),
        'number_of_sessions' => array(
            'rule'     => 'notEmpty',
            'required' => true
        ),
        'year' => array(
            'rule'     => 'notEmpty',
            'required' => true
        ),
        'free' => array(
            'rule'     => 'notEmpty',
            'required' => true
        )
    );
    
    public function getSelectOptions() {
        $selectOptions = array();
        
        $selectOptions['subjects']    = $this->Subject->find('list', array('fields' => array('Subject.name')));        
        $selectOptions['locations']   = $this->Location->find('list', array('fields' => array('Location.name')));
        $selectOptions['instructors'] = $this->Instructor1->find('list', array('fields' => array('Instructor1.name')));

        return $selectOptions;
    }
        
    public function prepareDataForEmail($data, $courseId) {
        // prepare values for letter
        $values = array();

        $values['firstName'] = $data['Student']['first_name'];
        $values['lastName']  = $data['Student']['last_name'];

        $contacts = array();
        if(!empty($data['Student']['home_phone'])) {
            $contacts[] = 'home phone - '.$data['Student']['home_phone'];
        }
        if(!empty($data['Student']['alt_phone'])) {
            $contacts[] = 'alternative phone - '.$data['Student']['alt_phone'];
        }
        if(!empty($data['Student']['email'])) {
            $contacts[] = 'email - '.$data['Student']['email'];
        }

        $values['contact'] = implode('; ', $contacts);
        
        $course = $this->find('first', array('conditions' => array('Course.id' => $courseId)));
        $values['course']  = $course['Course']['subject'].', ID - '.$courseId;
        
        return $values;
    }
    
    public function getEmailsTo() {
        $AdminUser = ClassRegistry::init('AdminUser');
        $emailsTo  = $AdminUser->find('list', array(
            'conditions' => array('AdminUser.group_id <=' => '2'),
            'fields' => array('email')
        ));
        
        return $emailsTo;
    }
}
