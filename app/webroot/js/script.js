$(document).ready(function() {
    // Spoiler
    $('#container').on('click', '.spoiler-btn', function (e) {
        e.preventDefault();
        $(this).parent().children('.spoiler-body').toggle('collapse');
    });
});
/*
<div class="spoiler">
    <div class="spoiler-btn">Btn</div>
    <div class="spoiler-body collapse">Tetx</div>
</div>
*/
