<div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-8">
        <h3>Thank You</h3>
        <p>You are registered for Session. Your status "pre-Enrolled". To change 
            the status to "enrolled" need to pay for session.</p>
        <div class="row">
            <div class="col-sm-6">
                <p>You can do this now or at the meeting</p>
            </div>
            <div class="col-sm-6">
                <?php echo $this->paypal->button(
                        'Pay Now',
                        array(
                            'amount'    => $this->Session->read('free'),
                            'item_name' => 'Pay for session '.$this->Session->read('name'),
                            'custom'    => $this->Session->read('Student.id'),
                            'return'    => $_SERVER['HTTP_HOST'].'/pages/after_payment',
                            'rm'        => 2,
                            'test'      => true
                        ),
                        array('class' => 'btn btn-success btn-lg')
                    );
                ?>
            </div>
        </div>
    </div>
</div>
