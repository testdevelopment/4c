<div class="row">
    <div class="col-sm-3">Left column for this page</div>
    <div class="col-sm-9">
        <table class="table width-auto">
            <tr>
                <td>Class name</td>
                <td><?php echo $course['Subject']['name']; ?></td>
            </tr>
            <tr>
                <td>Start date</td>
                <td><?php echo $course['Course']['start_date']; ?></td>
            </tr>
            <tr>
                <td>End date</td>
                <td><?php echo $course['Course']['end_date']; ?></td>
            </tr>
            <tr>
                <td>Class time</td>
                <td><?php echo $course['Course']['class_time']; ?></td>
            </tr>
            <tr>
                <td>Location</td>
                <td><?php echo $course['Location']['name']; ?></td>
            </tr>
            <tr>
                <td>Free</td>
                <td><?php echo $course['Course']['free']; ?></td>
            </tr>
        </table>

        <?php
        echo $this->Form->create('Student');
        echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
        echo $this->Form->input('home_phone', array('required' => false));
        echo $this->Form->input('alt_phone', array('required' => false));
        echo $this->Form->input('email', array('required' => false));

        echo $this->Form->submit('Register', array('class' => 'btn btn-success save-post'));
        echo $this->form->end();
        ?>
    </div>
</div>
