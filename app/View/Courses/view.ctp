<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">

        <table class="table width-auto">
            <tr>
                <td>Class name</td>
                <td><?php echo $course['Subject']['name']; ?></td>
            </tr>
            <tr>
                <td>Start date</td>
                <td><?php echo $course['Course']['start_date']; ?></td>
            </tr>
            <tr>
                <td>End date</td>
                <td><?php echo $course['Course']['end_date']; ?></td>
            </tr>
            <tr>
                <td>Number of sessions</td>
                <td><?php echo $course['Course']['number_of_sessions']; ?></td>
            </tr>
            <tr>
                <td>Class time</td>
                <td><?php echo $course['Course']['class_time']; ?></td>
            </tr>
            <tr>
                <td>Confirmation date</td>
                <td><?php echo $course['Course']['confirmation_date']; ?></td>
            </tr>
            <tr>
                <td>Maximum enrolment</td>
                <td><?php echo $course['Course']['maximum_enrolment']; ?></td>
            </tr>
            <tr>
                <td>Curent enrolment</td>
                <td><?php echo $course['Course']['curent_enrolment']; ?></td>
            </tr>
            <tr>
                <td>Instructor 1</td>
                <td><?php echo $course['Instructor1']['name']; ?></td>
            </tr>
            <tr>
                <td>Instructor 2</td>
                <td><?php echo $course['Instructor2']['name']; ?></td>
            </tr>
            <tr>
                <td>Location</td>
                <td><?php echo $course['Location']['name']; ?></td>
            </tr>
            <tr>
                <td>Confirmation instructions</td>
                <td><?php echo $course['Course']['confirmation_instructions']; ?></td>
            </tr>
            <tr>
                <td>Semester</td>
                <td><?php echo $course['Course']['semester']; ?></td>
            </tr>
            <tr>
                <td>Year</td>
                <td><?php echo $course['Course']['year']; ?></td>
            </tr>
            <tr>
                <td>Free</td>
                <td><?php echo $course['Course']['free']; ?></td>
            </tr>
        </table>
    </div>
    <div class="col-sm-3">
        <?php echo $this->Html->link('Register for this class',
                array('controller' => 'courses', 'action' => 'registrations', $course['Course']['id']),
                array('class' => 'btn btn-block btn-success')
            ); ?>
    </div>
</div>
