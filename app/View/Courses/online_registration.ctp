<div class="row">
    <div class="col-sm-3">Left column for this page</div>
    <div class="col-sm-9">
        <h3>Registration Form and Policies</h3>

        <table class="table width-auto">
            <tr>
                <td>Class name</td>
                <td><?php echo $course['Subject']['name']; ?></td>
            </tr>
            <tr>
                <td>Start date</td>
                <td><?php echo $course['Course']['start_date']; ?></td>
            </tr>
            <tr>
                <td>End date</td>
                <td><?php echo $course['Course']['end_date']; ?></td>
            </tr>
            <tr>
                <td>Class time</td>
                <td><?php echo $course['Course']['class_time']; ?></td>
            </tr>
            <tr>
                <td>Location</td>
                <td><?php echo $course['Location']['name']; ?></td>
            </tr>
            <tr>
                <td>Free</td>
                <td><?php echo $course['Course']['free']; ?></td>
            </tr>
        </table>
        
        <h4>Policies</h4>
        <ul>
            <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa 
                quae ab illo inventore veritatis et quasi architecto beatae vitae 
                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas 
                sit aspernatur aut odit aut fugit, sed quia consequuntur magni 
                dolores eos qui ratione voluptatem sequi nesciunt.</li>
            <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa 
                quae ab illo inventore veritatis et quasi architecto beatae vitae 
                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas 
                sit aspernatur aut odit aut fugit, sed quia consequuntur magni 
                dolores eos qui ratione voluptatem sequi nesciunt.</li>
            <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem 
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa 
                quae ab illo inventore veritatis et quasi architecto beatae vitae 
                dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas 
                sit aspernatur aut odit aut fugit, sed quia consequuntur magni 
                dolores eos qui ratione voluptatem sequi nesciunt.</li>
        </ul>
        
        <h4>Registration Form</h4>
        <?php
        echo $this->Form->create('Student');
        echo $this->Form->input('first_name');
        echo $this->Form->input('last_name');
        echo $this->Form->input('social_security_number');
        echo $this->Form->input('address');
        echo $this->Form->input('city');
        echo $this->Form->input('country');
        echo $this->Form->input('state');
        echo $this->Form->input('zip');
        echo $this->Form->input('home_phone', array('required' => false));
        echo $this->Form->input('alt_phone', array('required' => false));
        echo $this->Form->input('email', array('required' => false));
        echo $this->Form->input(
            '18_age',
            array(
                'type' => 'radio',
                'options' => array(1 => 'Yes', 0 => 'No'),
                'value' => 1,
                'legend' => '18 years of age or older'
            )
        );
        echo $this->Form->submit('Register', array('class' => 'btn btn-success save-post'));
        echo $this->form->end();
        ?>
    </div>
</div>

