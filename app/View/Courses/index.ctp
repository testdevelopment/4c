<div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-6">
        <table class="table table-striped width-auto">
            <tr>
                <th></th>
                <th>Session Name</th>
                <th>Start Date</th>
            </tr>
            <?php
                foreach ($courses as $course) {
                    echo '<tr>';
                    echo '<td>'.$this->Html->link('View', array('controller' => 'courses', 'action' => 'view', $course['Course']['id'])).'</td>';
                    echo '<td>'.$course['Subject']['name'].'</td>';
                    echo '<td>'.$course['Course']['start_date'].'</td>';
                    echo '</tr>';
                }
            ?>
        </table>
    </div>
</div>
