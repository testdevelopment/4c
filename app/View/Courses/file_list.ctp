<div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-6">
        <table class="table table-striped width-auto">
            <tr>
                <th>File</th>
                <th>Download</th>
            </tr>
            <?php
            foreach ($files as $file) {
                echo '<tr>';
                echo '<td>'.$file['AdminFile']['title'].'</td>';
                echo '<td class="td-horisontal-center">'.$this->Html->link(
                        '<span class="glyphicon glyphicon-download"></span>',
                        array(
                            'plugin' => 'admin',
                            'controller' => 'admin_files',
                            'action' => 'downloadFile',
                            $file['AdminFile']['name'],
                            $file['AdminFile']['title']
                        ),
                        array('escape' => false)
                    ).'</td>';
                echo '</tr>';
            }
            ?>
        </table>
    </div>
</div>

