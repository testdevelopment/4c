<div class="row">
    <div class="col-sm-3">
        <h3>Online Registrstion/Pre-registration</h3>
        <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium 
            voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi 
            sint occaecati cupiditate non provident, similique sunt in culpa qui 
            officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
        <?php echo $this->Html->link('Register',
                array('controller' => 'courses', 'action' => 'onlineRegistration'),
                array('class' => 'btn btn-block btn-success')
            ); ?>
    </div>
    <div class="col-sm-3">
        <h3>Downloadable Registration Forms</h3>
        <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium 
            voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi 
            sint occaecati cupiditate non provident, similique sunt in culpa qui 
            officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
        <?php echo $this->Html->link('Download',
                array('controller' => 'courses', 'action' => 'fileList'),
                array('class' => 'btn btn-block btn-success')
            ); ?>
    </div>
    <div class="col-sm-3">
        <h3>Contact Information Registration</h3>
        <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium 
            voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi 
            sint occaecati cupiditate non provident, similique sunt in culpa qui 
            officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
        <?php echo $this->Html->link('Register',
                array('controller' => 'courses', 'action' => 'contactRegistration'),
                array('class' => 'btn btn-block btn-success')
            ); ?>
    </div>
    <div class="col-sm-3">
        <h3>Register by phone</h3>
        <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium 
            voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi 
            sint occaecati cupiditate non provident, similique sunt in culpa qui 
            officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
    </div>
</div>

