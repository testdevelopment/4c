<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
    <title><?php echo $title_for_layout;?></title>
	<?php
            echo $this->Html->meta('icon');

            echo $this->Html->css('bootstrap');
            echo $this->Html->css('style');
            echo $this->Html->script('jquery.min');
            echo $this->Html->script('script');

            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->fetch('script');
	?>
</head>
<body>
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div id="container">
                <?php echo $this->element('header'); ?>
                <div id="content">
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->fetch('content'); ?>
                </div>
                <?php echo $this->element('footer'); ?>
            </div>
        </div>
    </div>
    <?php echo $this->element('sql_dump'); ?>
</body>
</html>
