-- phpMyAdmin SQL Dump
-- version 4.2.3deb1.precise~ppa.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Лис 04 2014 р., 11:45
-- Версія сервера: 5.5.40-0ubuntu0.12.04.1
-- Версія PHP: 5.5.18-1+deb.sury.org~precise+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База даних: `4c`
--

-- --------------------------------------------------------

--
-- Структура таблиці `admin_files`
--

CREATE TABLE IF NOT EXISTS `admin_files` (
`id` int(8) NOT NULL,
  `title` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `status` varchar(4) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Дамп даних таблиці `admin_files`
--

INSERT INTO `admin_files` (`id`, `title`, `name`, `status`) VALUES
(2, 'netbeans 8_0_1', '545633da2b2e6.desktop', 'Show');

-- --------------------------------------------------------

--
-- Структура таблиці `admin_users`
--

CREATE TABLE IF NOT EXISTS `admin_users` (
`id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` char(40) NOT NULL,
  `email` varchar(30) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `admin_users`
--

INSERT INTO `admin_users` (`id`, `name`, `login`, `password`, `email`, `group_id`, `created`, `modified`) VALUES
(1, 'administrator', 'admin', 'cb59901792d604d844312c648d885660568f8b17', 'orthener@ukr.net', 1, NULL, '2014-10-27 15:26:32'),
(2, 'Curator1', 'manager', 'cb59901792d604d844312c648d885660568f8b17', 'orthener@i.ua', 2, NULL, '2014-10-17 17:31:44'),
(3, 'Admin User lvl3', 'manager2', 'cb59901792d604d844312c648d885660568f8b17', 'orthener@ukr.net', 3, '2014-10-27 15:24:33', '2014-10-27 15:25:08');

-- --------------------------------------------------------

--
-- Структура таблиці `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
`id` int(10) unsigned NOT NULL,
  `subject_id` int(10) NOT NULL,
  `start_date` varchar(10) NOT NULL,
  `end_date` varchar(10) NOT NULL,
  `number_of_sessions` int(3) NOT NULL,
  `class_time` time NOT NULL,
  `confirmation_date` varchar(10) NOT NULL,
  `maximum_enrolment` int(3) NOT NULL,
  `curent_enrolment` int(3) NOT NULL,
  `instructor_1_id` int(10) NOT NULL,
  `instructor_2_id` int(10) NOT NULL,
  `location_id` int(10) NOT NULL,
  `confirmation_instructions` varchar(1000) NOT NULL,
  `semester` varchar(40) NOT NULL,
  `year` int(4) NOT NULL,
  `free` int(10) NOT NULL,
  `active` varchar(10) NOT NULL DEFAULT 'Inactive',
  `modified_by` varchar(40) NOT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп даних таблиці `courses`
--

INSERT INTO `courses` (`id`, `subject_id`, `start_date`, `end_date`, `number_of_sessions`, `class_time`, `confirmation_date`, `maximum_enrolment`, `curent_enrolment`, `instructor_1_id`, `instructor_2_id`, `location_id`, `confirmation_instructions`, `semester`, `year`, `free`, `active`, `modified_by`, `modified`) VALUES
(4, 4, '10/08/2014', '10/23/2014', 25, '18:30:00', '10/09/2014', 20, 20, 3, 2, 3, 'ÐŸÐµÑ€Ð²Ð°Ñ ÑÐµÑÑÐ¸Ñ', '2nd Quarter', 2014, 1100, 'Active', 'administrator', '2014-11-03 17:53:44'),
(5, 1, '10/16/2014', '10/10/2014', 25, '15:55:00', '10/02/2014', 20, 1, 2, 3, 1, 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.', '1st Quarter', 2014, 200, 'Active', 'administrator', '2014-11-03 17:55:29'),
(6, 5, '10/08/2014', '10/23/2014', 15, '11:00:00', '10/27/2014', 30, 2, 3, 3, 4, '', '1st Quarter', 2014, 100, 'Active', 'administrator', '2014-11-03 17:55:41');

-- --------------------------------------------------------

--
-- Структура таблиці `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `groups`
--

INSERT INTO `groups` (`id`, `name`) VALUES
(1, 'administrator'),
(2, 'curator lvl1'),
(3, 'curator lvl2');

-- --------------------------------------------------------

--
-- Структура таблиці `instant_payment_notifications`
--

CREATE TABLE IF NOT EXISTS `instant_payment_notifications` (
  `id` char(36) NOT NULL,
  `notify_version` varchar(64) DEFAULT NULL COMMENT 'IPN Version Number',
  `verify_sign` varchar(127) DEFAULT NULL COMMENT 'Encrypted string used to verify the authenticityof the tansaction',
  `test_ipn` int(11) DEFAULT NULL,
  `address_city` varchar(40) DEFAULT NULL COMMENT 'City of customers address',
  `address_country` varchar(64) DEFAULT NULL COMMENT 'Country of customers address',
  `address_country_code` varchar(2) DEFAULT NULL COMMENT 'Two character ISO 3166 country code',
  `address_name` varchar(128) DEFAULT NULL COMMENT 'Name used with address (included when customer provides a Gift address)',
  `address_state` varchar(40) DEFAULT NULL COMMENT 'State of customer address',
  `address_status` varchar(20) DEFAULT NULL COMMENT 'confirmed/unconfirmed',
  `address_street` varchar(200) DEFAULT NULL COMMENT 'Customer''s street address',
  `address_zip` varchar(20) DEFAULT NULL COMMENT 'Zip code of customer''s address',
  `first_name` varchar(64) DEFAULT NULL COMMENT 'Customer''s first name',
  `last_name` varchar(64) DEFAULT NULL COMMENT 'Customer''s last name',
  `payer_business_name` varchar(127) DEFAULT NULL COMMENT 'Customer''s company name, if customer represents a business',
  `payer_email` varchar(127) DEFAULT NULL COMMENT 'Customer''s primary email address. Use this email to provide any credits',
  `payer_id` varchar(13) DEFAULT NULL COMMENT 'Unique customer ID.',
  `payer_status` varchar(20) DEFAULT NULL COMMENT 'verified/unverified',
  `contact_phone` varchar(20) DEFAULT NULL COMMENT 'Customer''s telephone number.',
  `residence_country` varchar(2) DEFAULT NULL COMMENT 'Two-Character ISO 3166 country code',
  `business` varchar(127) DEFAULT NULL COMMENT 'Email address or account ID of the payment recipient (that is, the merchant). Equivalent to the values of receiver_email (If payment is sent to primary account) and business set in the Website Payment HTML.',
  `item_name` varchar(127) DEFAULT NULL COMMENT 'Item name as passed by you, the merchant. Or, if not passed by you, as entered by your customer. If this is a shopping cart transaction, Paypal will append the number of the item (e.g., item_name_1,item_name_2, and so forth).',
  `item_number` varchar(127) DEFAULT NULL COMMENT 'Pass-through variable for you to track purchases. It will get passed back to you at the completion of the payment. If omitted, no variable will be passed back to you.',
  `quantity` varchar(127) DEFAULT NULL COMMENT 'Quantity as entered by your customer or as passed by you, the merchant. If this is a shopping cart transaction, PayPal appends the number of the item (e.g., quantity1,quantity2).',
  `receiver_email` varchar(127) DEFAULT NULL COMMENT 'Primary email address of the payment recipient (that is, the merchant). If the payment is sent to a non-primary email address on your PayPal account, the receiver_email is still your primary email.',
  `receiver_id` varchar(13) DEFAULT NULL COMMENT 'Unique account ID of the payment recipient (i.e., the merchant). This is the same as the recipients referral ID.',
  `custom` varchar(255) DEFAULT NULL COMMENT 'Custom value as passed by you, the merchant. These are pass-through variables that are never presented to your customer.',
  `invoice` varchar(127) DEFAULT NULL COMMENT 'Pass through variable you can use to identify your invoice number for this purchase. If omitted, no variable is passed back.',
  `memo` varchar(255) DEFAULT NULL COMMENT 'Memo as entered by your customer in PayPal Website Payments note field.',
  `option_name1` varchar(64) DEFAULT NULL COMMENT 'Option name 1 as requested by you',
  `option_name2` varchar(64) DEFAULT NULL COMMENT 'Option 2 name as requested by you',
  `option_selection1` varchar(200) DEFAULT NULL COMMENT 'Option 1 choice as entered by your customer',
  `option_selection2` varchar(200) DEFAULT NULL COMMENT 'Option 2 choice as entered by your customer',
  `tax` decimal(10,2) DEFAULT NULL COMMENT 'Amount of tax charged on payment',
  `auth_id` varchar(19) DEFAULT NULL COMMENT 'Authorization identification number',
  `auth_exp` varchar(28) DEFAULT NULL COMMENT 'Authorization expiration date and time, in the following format: HH:MM:SS DD Mmm YY, YYYY PST',
  `auth_amount` int(11) DEFAULT NULL COMMENT 'Authorization amount',
  `auth_status` varchar(20) DEFAULT NULL COMMENT 'Status of authorization',
  `num_cart_items` int(11) DEFAULT NULL COMMENT 'If this is a PayPal shopping cart transaction, number of items in the cart',
  `parent_txn_id` varchar(19) DEFAULT NULL COMMENT 'In the case of a refund, reversal, or cancelled reversal, this variable contains the txn_id of the original transaction, while txn_id contains a new ID for the new transaction.',
  `payment_date` varchar(28) DEFAULT NULL COMMENT 'Time/date stamp generated by PayPal, in the following format: HH:MM:SS DD Mmm YY, YYYY PST',
  `payment_status` varchar(20) DEFAULT NULL COMMENT 'Payment status of the payment',
  `payment_type` varchar(10) DEFAULT NULL COMMENT 'echeck/instant',
  `pending_reason` varchar(20) DEFAULT NULL COMMENT 'This variable is only set if payment_status=pending',
  `reason_code` varchar(20) DEFAULT NULL COMMENT 'This variable is only set if payment_status=reversed',
  `remaining_settle` int(11) DEFAULT NULL COMMENT 'Remaining amount that can be captured with Authorization and Capture',
  `shipping_method` varchar(64) DEFAULT NULL COMMENT 'The name of a shipping method from the shipping calculations section of the merchants account profile. The buyer selected the named shipping method for this transaction',
  `shipping` decimal(10,2) DEFAULT NULL COMMENT 'Shipping charges associated with this transaction. Format unsigned, no currency symbol, two decimal places',
  `transaction_entity` varchar(20) DEFAULT NULL COMMENT 'Authorization and capture transaction entity',
  `txn_id` varchar(19) DEFAULT '' COMMENT 'A unique transaction ID generated by PayPal',
  `txn_type` varchar(20) DEFAULT NULL COMMENT 'cart/express_checkout/send-money/virtual-terminal/web-accept',
  `exchange_rate` decimal(10,2) DEFAULT NULL COMMENT 'Exchange rate used if a currency conversion occured',
  `mc_currency` varchar(3) DEFAULT NULL COMMENT 'Three character country code. For payment IPN notifications, this is the currency of the payment, for non-payment subscription IPN notifications, this is the currency of the subscription.',
  `mc_fee` decimal(10,2) DEFAULT NULL COMMENT 'Transaction fee associated with the payment, mc_gross minus mc_fee equals the amount deposited into the receiver_email account. Equivalent to payment_fee for USD payments. If this amount is negative, it signifies a refund or reversal, and either ofthose p',
  `mc_gross` decimal(10,2) DEFAULT NULL COMMENT 'Full amount of the customer''s payment',
  `mc_handling` decimal(10,2) DEFAULT NULL COMMENT 'Total handling charge associated with the transaction',
  `mc_shipping` decimal(10,2) DEFAULT NULL COMMENT 'Total shipping amount associated with the transaction',
  `payment_fee` decimal(10,2) DEFAULT NULL COMMENT 'USD transaction fee associated with the payment',
  `payment_gross` decimal(10,2) DEFAULT NULL COMMENT 'Full USD amount of the customers payment transaction, before payment_fee is subtracted',
  `settle_amount` decimal(10,2) DEFAULT NULL COMMENT 'Amount that is deposited into the account''s primary balance after a currency conversion',
  `settle_currency` varchar(3) DEFAULT NULL COMMENT 'Currency of settle amount. Three digit currency code',
  `auction_buyer_id` varchar(64) DEFAULT NULL COMMENT 'The customer''s auction ID.',
  `auction_closing_date` varchar(28) DEFAULT NULL COMMENT 'The auction''s close date. In the format: HH:MM:SS DD Mmm YY, YYYY PSD',
  `auction_multi_item` int(11) DEFAULT NULL COMMENT 'The number of items purchased in multi-item auction payments',
  `for_auction` varchar(10) DEFAULT NULL COMMENT 'This is an auction payment - payments made using Pay for eBay Items or Smart Logos - as well as send money/money request payments with the type eBay items or Auction Goods(non-eBay)',
  `subscr_date` varchar(28) DEFAULT NULL COMMENT 'Start date or cancellation date depending on whether txn_type is subcr_signup or subscr_cancel',
  `subscr_effective` varchar(28) DEFAULT NULL COMMENT 'Date when a subscription modification becomes effective',
  `period1` varchar(10) DEFAULT NULL COMMENT '(Optional) Trial subscription interval in days, weeks, months, years (example a 4 day interval is 4 D',
  `period2` varchar(10) DEFAULT NULL COMMENT '(Optional) Trial period',
  `period3` varchar(10) DEFAULT NULL COMMENT 'Regular subscription interval in days, weeks, months, years',
  `amount1` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for Trial period 1 for USD',
  `amount2` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for Trial period 2 for USD',
  `amount3` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for regular subscription  period 1 for USD',
  `mc_amount1` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for trial period 1 regardless of currency',
  `mc_amount2` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for trial period 2 regardless of currency',
  `mc_amount3` decimal(10,2) DEFAULT NULL COMMENT 'Amount of payment for regular subscription period regardless of currency',
  `recurring` varchar(1) DEFAULT NULL COMMENT 'Indicates whether rate recurs (1 is yes, blank is no)',
  `reattempt` varchar(1) DEFAULT NULL COMMENT 'Indicates whether reattempts should occur on payment failure (1 is yes, blank is no)',
  `retry_at` varchar(28) DEFAULT NULL COMMENT 'Date PayPal will retry a failed subscription payment',
  `recur_times` int(11) DEFAULT NULL COMMENT 'The number of payment installations that will occur at the regular rate',
  `username` varchar(64) DEFAULT NULL COMMENT '(Optional) Username generated by PayPal and given to subscriber to access the subscription',
  `password` varchar(24) DEFAULT NULL COMMENT '(Optional) Password generated by PayPal and given to subscriber to access the subscription (Encrypted)',
  `subscr_id` varchar(19) DEFAULT NULL COMMENT 'ID generated by PayPal for the subscriber',
  `case_id` varchar(28) DEFAULT NULL COMMENT 'Case identification number',
  `case_type` varchar(28) DEFAULT NULL COMMENT 'complaint/chargeback',
  `case_creation_date` varchar(28) DEFAULT NULL COMMENT 'Date/Time the case was registered',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблиці `instructors`
--

CREATE TABLE IF NOT EXISTS `instructors` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `instructors`
--

INSERT INTO `instructors` (`id`, `name`) VALUES
(2, 'Instructor 2'),
(3, 'Instructor-1');

-- --------------------------------------------------------

--
-- Структура таблиці `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(40) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Дамп даних таблиці `locations`
--

INSERT INTO `locations` (`id`, `name`) VALUES
(1, 'kabinet 225'),
(3, 'Class 13'),
(4, 'Kabinet 1');

-- --------------------------------------------------------

--
-- Структура таблиці `paypal_items`
--

CREATE TABLE IF NOT EXISTS `paypal_items` (
  `id` varchar(36) NOT NULL,
  `instant_payment_notification_id` varchar(36) NOT NULL,
  `item_name` varchar(127) DEFAULT NULL,
  `item_number` varchar(127) DEFAULT NULL,
  `quantity` varchar(127) DEFAULT NULL,
  `mc_gross` float(10,2) DEFAULT NULL,
  `mc_shipping` float(10,2) DEFAULT NULL,
  `mc_handling` float(10,2) DEFAULT NULL,
  `tax` float(10,2) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблиці `students`
--

CREATE TABLE IF NOT EXISTS `students` (
`id` int(10) unsigned NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `social_security_number` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(40) NOT NULL,
  `state` varchar(40) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `home_phone` varchar(20) NOT NULL,
  `alt_phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `18_age` int(1) NOT NULL,
  `enrollment_status` varchar(15) NOT NULL,
  `registration_date` varchar(10) NOT NULL,
  `enrolled_date` varchar(10) NOT NULL,
  `amount_paid` int(5) DEFAULT NULL,
  `paid_date` varchar(10) NOT NULL,
  `transaction_id` varchar(20) NOT NULL,
  `course_id` int(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Дамп даних таблиці `students`
--

INSERT INTO `students` (`id`, `first_name`, `last_name`, `social_security_number`, `address`, `city`, `country`, `state`, `zip`, `home_phone`, `alt_phone`, `email`, `18_age`, `enrollment_status`, `registration_date`, `enrolled_date`, `amount_paid`, `paid_date`, `transaction_id`, `course_id`) VALUES
(26, 'First Name 2', 'Last Name 2', '4456', 'Address bla bla', 'Herson', 'Ukraine', 'Herson', '73028', '456789', '', '', 1, '', '', '', 0, '', '', 0),
(27, 'First_Name 2', 'Last_Name 2', '55', 'Address bla bla', 'Herson', 'Ukraine', 'Herson', '73028', '', '', 'orthener@i.ua', 0, 'Enrolled', '10/27/2014', '', 101, '', '', 5),
(28, 'First Name 2', 'Last Name 2', '7', 'Address bla bla', 'Herson', 'Ukraine', 'Herson', '73028', '456789', '', '', 1, 'Enrolled', '10/27/2014', '', 0, '', '', 5),
(30, 'First Name 3', 'Last Name 3', '5', 'Address bla bla', 'Herson', 'Ukraine', 'Herson', '73028', '456789', '', '', 1, 'Cancelled', '10/28/2014', '', 0, '', '', 4),
(31, 'First Name 3', 'Last Name 3', '5', 'Address bla bla', 'Herson', 'Ukraine', 'Herson', '', '', '456789', '', 1, 'Cancelled', '10/28/2014', '10/31/2014', 0, '', '', 4),
(32, 'First NAme', 'Last_Name 2', '', 'Address bla bla', '', '', '', '', '456789', '', '', 1, 'Cancelled', '10/28/2014', '', 0, '', '', 4),
(33, 'First Name 4', 'Last_Name 4', '456', '', '', '', '', '', '', '', 'orthener@i.ua', 0, 'Enrolled', '10/29/2014', '', 10, '10/29/2014', '', 6),
(34, 'First Name 4', 'Last_Name 4', '', '', '', '', '', '', '', '', 'orthener@i.ua', 1, 'Enrolled', '10/29/2014', '10/29/2014', NULL, '', '', 4),
(35, 'First NAme', 'Last Name', '456', 'Address bla bla', 'Herson', 'Ukraine', '', '73000', '', '456789', 'orthener@i.ua', 1, 'Enrolled', '10/29/2014', '10/29/2014', NULL, '', '', 6),
(36, 'First NAme', 'Last Name', '456', '', '', '', '', '', '', '', 'orthener@i.ua', 1, 'Enrolled', '10/29/2014', '10/31/2014', NULL, '', '', 4),
(37, 'First NAme', 'Last Name', '456', '', '', '', '', '', '', '', 'orthener@i.ua', 1, 'Cancelled', '10/29/2014', '', NULL, '', '', 4),
(38, 'First NAme', 'Last Name', '666', 'Krimska', 'Herson', '', '', '', '', '', 'orthener@i.ua', 0, 'Enrolled', '10/29/2014', '10/31/2014', NULL, '', '', 6),
(39, 'First NAme', 'Last Name', '666', 'Krimska', 'Herson', '', '', '', '', '', 'orthener@i.ua', 0, 'Enrolled', '10/29/2014', '10/31/2014', NULL, '', '', 6),
(40, 'First NAme', 'Last Name', '55', '', '', '', '', '', '456789', '', 'orthener@i.ua', 1, 'Pre-Enrolled', '10/30/2014', '', NULL, '', '', 4),
(41, 'First NAme', 'Last Name', '456', '', '', '', '', '', '456789', '', '', 1, 'Pre-Enrolled', '10/31/2014', '', NULL, '', '', 4),
(42, 'First NAme', 'Last Name', '456', '', '', '', '', '', '456789', '', '', 1, 'Pre-Enrolled', '10/31/2014', '', NULL, '', '', 4),
(43, 'First NAme', 'Last Name', '456', '', '', '', '', '', '456789', '', '', 1, 'Pre-Enrolled', '10/31/2014', '', NULL, '', '', 4),
(44, 'First NAme', 'Last Name', '456', '', '', '', '', '', '456789', '', '', 1, 'Pre-Enrolled', '10/31/2014', '', NULL, '', '', 4),
(45, 'First NAme', 'Last Name', '456', '', '', '', '', '', '456789', '', '', 1, 'Pre-Enrolled', '10/31/2014', '', NULL, '', '', 4),
(46, 'First NAme', 'Last Name', '456', '', '', '', '', '', '456789', '', '', 1, 'Pre-Enrolled', '10/31/2014', '', NULL, '', '', 4),
(47, 'First NAme', 'Last Name', '456', '', '', '', '', '', '456789', '', '', 1, 'Pre-Enrolled', '10/31/2014', '', NULL, '', '', 4),
(48, 'First Name 2', 'Last Name 2', '', '', '', '', '', '', '', '', 'orthener@i.ua', 1, 'Pre-Enrolled', '10/31/2014', '', NULL, '', '', 4),
(49, 'First Name 2', 'Last Name 2', '556', '', '', '', '', '', '', '456789', '', 1, 'Enrolled', '10/31/2014', '10/31/2014', NULL, '', '', 4);

-- --------------------------------------------------------

--
-- Структура таблиці `student_histories`
--

CREATE TABLE IF NOT EXISTS `student_histories` (
`id` int(10) NOT NULL,
  `text` varchar(100) NOT NULL,
  `student_id` int(10) NOT NULL,
  `modified` date NOT NULL,
  `modified_by` varchar(40) NOT NULL DEFAULT 'System'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Дамп даних таблиці `student_histories`
--

INSERT INTO `student_histories` (`id`, `text`, `student_id`, `modified`, `modified_by`) VALUES
(1, 'first item', 30, '0000-00-00', 'System'),
(4, 'one more tip', 30, '2014-10-28', 'administrator'),
(5, 'one more item', 30, '2014-10-29', 'administrator'),
(6, 'one more item', 30, '2014-10-29', 'administrator'),
(7, 'tip for 27', 27, '2014-10-29', 'administrator'),
(8, 'one more tip for 27', 27, '2014-10-29', 'administrator'),
(9, 'test', 34, '2014-10-29', 'test'),
(10, 'Student added from admin panel', 35, '2014-10-29', 'administrator'),
(11, 'Student added from admin panel', 37, '2014-10-29', 'administrator'),
(12, 'Student cencelled', 37, '2014-10-29', 'administrator'),
(13, 'Student enrolled', 35, '2014-10-29', 'administrator'),
(14, 'Pre-enrollment submited from website', 39, '2014-10-29', 'System'),
(15, 'Pre-enrollment submited from website', 40, '2014-10-30', 'System'),
(16, 'Pre-enrollment submited from website', 41, '2014-10-31', 'System'),
(17, 'Pre-enrollment submited from website', 42, '2014-10-31', 'System'),
(18, 'Pre-enrollment submited from website', 43, '2014-10-31', 'System'),
(19, 'Pre-enrollment submited from website', 44, '2014-10-31', 'System'),
(20, 'Pre-enrollment submited from website', 45, '2014-10-31', 'System'),
(21, 'Pre-enrollment submited from website', 46, '2014-10-31', 'System'),
(22, 'Pre-enrollment submited from website', 47, '2014-10-31', 'System'),
(23, 'Pre-enrollment submited from website', 48, '2014-10-31', 'System'),
(24, 'Student enrolled', 31, '2014-10-31', 'administrator'),
(25, 'Student enrolled', 36, '2014-10-31', 'administrator'),
(26, 'Student enrolled', 38, '2014-10-31', 'administrator'),
(27, 'Student enrolled', 39, '2014-10-31', 'administrator'),
(28, 'Student cencelled', 30, '2014-10-31', 'administrator'),
(29, 'Student cencelled', 31, '2014-10-31', 'administrator'),
(30, 'Student added from admin panel', 49, '2014-10-31', 'administrator');

-- --------------------------------------------------------

--
-- Структура таблиці `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
`id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп даних таблиці `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `description`) VALUES
(1, 'Class', 'dqopdf;qqdmqkl'),
(4, 'Class2', ''),
(5, 'Class 3', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_files`
--
ALTER TABLE `admin_files`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`name`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instant_payment_notifications`
--
ALTER TABLE `instant_payment_notifications`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructors`
--
ALTER TABLE `instructors`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paypal_items`
--
ALTER TABLE `paypal_items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_histories`
--
ALTER TABLE `student_histories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_files`
--
ALTER TABLE `admin_files`
MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `instructors`
--
ALTER TABLE `instructors`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `student_histories`
--
ALTER TABLE `student_histories`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
